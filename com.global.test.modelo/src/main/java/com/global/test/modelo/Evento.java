package com.global.test.modelo;

import java.util.Date;

/**
 *
 * Evento
 *
 *
 * @author ChrisV_G
 * @since 12/05/2017
 *
 *
 * Historia de Modificaciones --------------------------------------------------
 * Autor Fecha Modificacion ----------------- -------------- ------------------
 */
public class Evento {

    /**
     * id del evento
     */
    private Long idEvento;
    /**
     * nombre del evento
     */
    private String nombre;
    /**
     * fecha del evento
     */
    private Date fecha;
    /**
     * id del evento
     */
    private String lugar;
    /**
     * id del evento
     */
    private Categoria categoriaID;

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Categoria getCategoriaID() {
        return categoriaID;
    }

    public void setCategoriaID(Categoria categoriaID) {
        this.categoriaID = categoriaID;
    }

    @Override
    public String toString() {
        return "Evento{" + "idEvento=" + idEvento + ", nombre=" + nombre + ", fecha=" + fecha + ", lugar=" + lugar + ", categoriaID=" + categoriaID + '}';
    }

 
    
}
