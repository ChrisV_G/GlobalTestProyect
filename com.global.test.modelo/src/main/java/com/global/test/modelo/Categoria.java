package com.global.test.modelo;

import java.util.List;

/**
 *
 * Categoria
 *
 *
 * @author ChrisV_G
 * @since 12/05/2017
 *
 *
 * Historia de Modificaciones --------------------------------------------------
 * Autor Fecha Modificacion ----------------- -------------- ------------------
 */
public class Categoria {

    /**
     * id de la categoria
     */
    private Long idCategoria;
    /**
     * nonmbre de la categoria
     */
    private String nombre;

    /**
     * estado de la categoria
     */
    private String estado;
    /**
     * eventos relacionados a la categoria
     */
    private List<Evento> eventos;

    public Categoria() {
        this.nombre = "";
        this.estado = "";

    }

    public Long getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Evento> getEventos() {
        return eventos;
    }

    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

    @Override
    public String toString() {
        return "Categoria{" + "idCategoria=" + idCategoria + ", nombre=" + nombre + ", estado=" + estado + ", eventos=" + eventos + '}';
    }

}
