
import com.global.test.modelo.Categoria;
import com.global.test.modelo.Categoria;
import com.global.test.persistence.dao.CategoriaDAO;
import java.util.Date;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * CategoriaDAOTest
 *
 *
 * @author ChrisV_G
 * @since 15/05/2017
 *
 *
 * Historia de Modificaciones --------------------------------------------------
 * Autor Fecha Modificacion ----------------- -------------- ------------------
 */
public class CategoriaDAOTest extends TestCase {

    @Test
    public void testConsultarTodos() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("web-context-test.xml");
        CategoriaDAO categoriaDAO = (CategoriaDAO) context.getBean("categoriaDAO");
        Assert.assertEquals(true, categoriaDAO.selectAll().size() > 0);
    }

    @Test
    public void testGuardarCategoria() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("web-context-test.xml");
        CategoriaDAO categoriaDAO = (CategoriaDAO) context.getBean("categoriaDAO");
        Categoria categoriaTest = new Categoria();
        categoriaTest.setEstado("1");
        categoriaTest.setNombre("CategoriaTest");
        Assert.assertEquals(true, categoriaDAO.persist(categoriaTest));

    }

    @Test
    public void testActualizarCategoria() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("web-context-test.xml");
        CategoriaDAO categoriaDAO = (CategoriaDAO) context.getBean("categoriaDAO");
        Categoria categoriaTest = categoriaDAO.selectByname("CategoriaTest");
        categoriaTest.setEstado("0");
        categoriaTest.setNombre("CategoriaTestMerge");
        Assert.assertEquals(true, categoriaDAO.merge(categoriaTest));
    }

    @Test
    public void testDeleteCategoria() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("web-context-test.xml");
        CategoriaDAO categoriaDAO = (CategoriaDAO) context.getBean("categoriaDAO");
        Assert.assertEquals(true, categoriaDAO.delete(categoriaDAO.selectByname("CategoriaTest")));
    }

}
