
import com.global.test.modelo.Categoria;
import com.global.test.modelo.Evento;
import com.global.test.persistence.dao.EventoDAO;
import java.util.Date;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * EventoDAOTest
 *
 *
 * @author ChrisV_G
 * @since 15/05/2017
 *
 *
 * Historia de Modificaciones --------------------------------------------------
 * Autor Fecha Modificacion ----------------- -------------- ------------------
 */
public class EventoDAOTest extends TestCase {
 
    @Test
    public void testConsultarTodos() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("web-context-test.xml");
        EventoDAO eventoDAO = (EventoDAO) context.getBean("eventoDAO");
        Assert.assertEquals(true, eventoDAO.selectAll().size() > 0);
    }

    @Test
    public void testGuardarEvento() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("web-context-test.xml");
        EventoDAO eventoDAO = (EventoDAO) context.getBean("eventoDAO");
        Evento eventoTest = new Evento();
        Categoria cat = new Categoria();
        cat.setIdCategoria(1L);
        eventoTest.setCategoriaID(cat);
        eventoTest.setFecha(new Date());
        eventoTest.setLugar("Cucuta");
        eventoTest.setNombre("EventoTest");
        Assert.assertEquals(true, eventoDAO.persist(eventoTest));
        
    }

    @Test
    public void testActualizarEvento() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("web-context-test.xml");
        EventoDAO eventoDAO = (EventoDAO) context.getBean("eventoDAO");
        Evento eventoTest = eventoDAO.selectByname("EventoTest");
        eventoTest.setFecha(new Date());
        eventoTest.setLugar("CucutaTestMerge");
        eventoTest.setNombre("EventoTestMerge");
        System.out.println(eventoTest);
        Assert.assertEquals(true, eventoDAO.merge(eventoTest));
    }
    @Test
    public void testDeleteEvento() throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("web-context-test.xml");
        EventoDAO eventoDAO = (EventoDAO) context.getBean("eventoDAO"); 
        Assert.assertEquals(true, eventoDAO.delete(eventoDAO.selectByname("EventoTest")));
    }

}
