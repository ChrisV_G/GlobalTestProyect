
package com.global.test.web;

/**
 *
 * SessBean
 *
 *
 * @author ChrisV_G
 * @since 14/05/2017
 *
 *
 * Historia de Modificaciones
 * -------------------------------------------------- 
 * Autor             Fecha          Modificacion 
 * ----------------- -------------- ------------------
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */ 
 
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Christian
 */
@ManagedBean
@SessionScoped
public class SessBean implements Serializable {

    private boolean verFiltros;
    private boolean nuevoGrupo;
    private boolean verLog = true;
 
    @PostConstruct
    public void init() {
        verFiltros = false;
        verLog = true;

    } 
    public void logout() {


        try {
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().remove("sessBean");
            context.getExternalContext().getSessionMap().remove("treeBean");
            context.getExternalContext().getSessionMap().remove("beanVistas");
            ExternalContext ctx =
                    FacesContext.getCurrentInstance().getExternalContext();
            String ctxPath =
                    ((ServletContext) ctx.getContext()).getContextPath();
            // Usar el contexto de JSF para invalidar la sesión,
            // NO EL DE SERVLETS (nada de HttpServletRequest)
            ((HttpSession) ctx.getSession(false)).invalidate();

            // Redirección de nuevo con el contexto de JSF,
            // si se usa una HttpServletResponse fallará.
            // Sin embargo, como ya está fuera del ciclo de vida
            // de JSF se debe usar la ruta completa -_-U
            //ctx.redirect(ctxPath + "/inicio.jsf");
            ctx.redirect(ctxPath);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String valor() {
        return "";
    }

    public boolean isVerFiltros() {
        return verFiltros;
    }

    public void setVerFiltros(boolean verFiltros) {
        this.verFiltros = verFiltros;
    }

     

    public boolean isVerLog() {
        return verLog;
    }

    public void setVerLog(boolean verLog) {
        this.verLog = verLog;
    }

    public boolean isNuevoGrupo() {
        return nuevoGrupo;
    }

    public void setNuevoGrupo(boolean nuevoGrupo) {
        this.nuevoGrupo = nuevoGrupo;
    }
}
