package com.global.test.web;

import com.global.test.modelo.Categoria;
import com.global.test.persistence.services.CategoriaService;
import com.global.test.persistence.services.EventoService;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * CategoriaBean
 *
 *
 * @author ChrisV_G
 * @since 14/05/2017
 *
 *
 * Historia de Modificaciones --------------------------------------------------
 * Autor Fecha Modificacion ----------------- -------------- ------------------
 */
@ManagedBean
@ViewScoped
public class CategoriaBean implements Serializable {

    @ManagedProperty("#{categoriaService}")
    private CategoriaService categoriaService;
    @ManagedProperty("#{eventoService}")
    private EventoService eventoService;

    private List<Categoria> categoriaList;

    private Categoria categoria;
    private boolean edit;
    private boolean form;

    @PostConstruct
    public void init() {
        try {
            this.edit = false;
            categoria = new Categoria();
            categoriaList = categoriaService.retrieveAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void seachEventosByCategoria() throws Exception {
        categoria.setEventos(eventoService.retrieveByCategoria(categoria.getIdCategoria()+""));
    }

    public void eliminarItem() {
        if (categoria.getEventos().isEmpty()) {

            try {
                categoriaService.delete(categoria);
                FacesMessage msg = new FacesMessage("Registro Eliminado Exitosamente");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                this.edit = false;
                init();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            FacesMessage msg = new FacesMessage("No se pudo eliminar este item debido a que tiene referencias asociadas");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void verNuevoModulo() {
        edit = false;
        categoria = new Categoria();
        System.out.println("ENTRA A LA ACCION");

        verForm();

    }

    public void reset() {

        init();
        this.edit = false;
        FacesMessage msg = new FacesMessage("Puede Ingresar Un Nuevo Registro");
        FacesContext.getCurrentInstance().addMessage("messages", msg);
    }

    public String aceptar() {

        try {
            if (edit) {
                categoriaService.merge(categoria);
                FacesMessage msg = new FacesMessage("Categoria Actualizada Exitosamente");
                FacesContext.getCurrentInstance().addMessage("messages", msg);
                init();
            } else {
                categoriaService.persist(categoria);
                FacesMessage msg = new FacesMessage("Categoria Creada Exitosamente");
                FacesContext.getCurrentInstance().addMessage("messages", msg);
                init();
            }

        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR AL GUARDAR " + e.getMessage(), "ERROR AL GUARDAR " + e.getMessage());
            FacesContext.getCurrentInstance().addMessage("messages", msg);
            init();
            e.printStackTrace();
        }
        return "";
    }

    /*metodos de ayuda*/
    public String convertirFecha(Date fecha) {
        if (fecha == null) {
            return "";
        } else {
            String patron = "dd/MM/yyyy";
            SimpleDateFormat formato = new SimpleDateFormat(patron);
            return (formato.format(fecha));
        }
    }

    public String convertirEstado(String estado) {
        if ("0".equals(estado)) {
            return "Inactivo";
        } else {
            return "Activo";
        }
    }

    public void verForm() {
        form = true;
    }

    public void verTabla() {
        form = false;
    }

    public void renderizarItem() {
        edit = true;
    }

    public CategoriaService getCategoriaService() {
        return categoriaService;
    }

    public void setCategoriaService(CategoriaService categoriaService) {
        this.categoriaService = categoriaService;
    }

    public List<Categoria> getCategoriaList() {
        return categoriaList;
    }

    public void setCategoriaList(List<Categoria> categoriaList) {
        this.categoriaList = categoriaList;
    }

    public Categoria getCategoria() {
        try {
            if (categoria == null) {
                categoria = new Categoria();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("Error inicializando objeto");
        }
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isForm() {
        return form;
    }

    public void setForm(boolean form) {
        this.form = form;
    }

    public EventoService getEventoService() {
        return eventoService;
    }

    public void setEventoService(EventoService eventoService) {
        this.eventoService = eventoService;
    }

}
