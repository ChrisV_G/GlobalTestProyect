
package com.global.test.web;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * NavigatorBean
 *
 *
 * @author ChrisV_G
 * @since 14/05/2017
 *
 *
 * Historia de Modificaciones
 * -------------------------------------------------- 
 * Autor             Fecha          Modificacion 
 * ----------------- -------------- ------------------
 */
@ManagedBean 
@RequestScoped

public class NavigatorBean implements Serializable {

   
   public String showPage(String pageId) {
      if(pageId == null) {
         return "/views/categorias.xhtml";
      }
      
      if(pageId.equals("1")) {
         return "/views/categorias.xhtml";
      }else if(pageId.equals("2")) {
         return "/views/eventos.xhtml";
      }else {
         return "/";
      }
   }

}