package com.global.test.web;

import com.global.test.modelo.Categoria;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.global.test.modelo.Evento;
import com.global.test.persistence.services.CategoriaService;
import com.global.test.persistence.services.EventoService;
import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * EventoBean
 *
 *
 * @author ChrisV_G
 * @since 14/05/2017
 *
 *
 * Historia de Modificaciones --------------------------------------------------
 * Autor Fecha Modificacion ----------------- -------------- ------------------
 */
@ManagedBean
@ViewScoped
public class EventoBean {

    @ManagedProperty("#{eventoService}")
    private EventoService eventoService;

    @ManagedProperty("#{categoriaService}")
    private CategoriaService categoriaService;

    private List<Evento> eventoList;
    private int idCategoria;
    private Categoria categoria;
    private List<SelectItem> itemsCategoria;
    private Evento evento;
    private boolean edit;
    private boolean form;

    @PostConstruct
    public void init() {
        try {
            this.edit = false;
            evento = new Evento();
            eventoList = eventoService.retrieveAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        armarItems();
    }

    private void armarItems() {
        try {
            itemsCategoria = new ArrayList<SelectItem>();
            itemsCategoria.clear();
            for (Categoria p : categoriaService.retrieveAll()) {
                itemsCategoria.add(new SelectItem(p.getIdCategoria(), p.getNombre()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void eliminarItem() {
        try {
            eventoService.delete(evento);
            FacesMessage msg = new FacesMessage("Registro Eliminado Exitosamente");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            this.edit = false;
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verNuevoModulo() {
        edit = false;
        evento = new Evento();
        verForm();
    }

    public void reset() {

        init();
        this.edit = false;
        FacesMessage msg = new FacesMessage("Puede Ingresar Un Nuevo Registro");
        FacesContext.getCurrentInstance().addMessage("messages", msg);
    }

    public String aceptar() {
        
        try {
            evento.setCategoriaID(categoriaService.findByID(idCategoria));
            if (edit) {
                eventoService.merge(evento);
                FacesMessage msg = new FacesMessage("Evento Actualizada Exitosamente");
                FacesContext.getCurrentInstance().addMessage("messages", msg);
                init();
            } else {
                eventoService.persist(evento);
                FacesMessage msg = new FacesMessage("Evento Creada Exitosamente");
                FacesContext.getCurrentInstance().addMessage("messages", msg);
                init();
            }

        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR AL GUARDAR " + e.getMessage(), "ERROR AL GUARDAR " + e.getMessage());
            FacesContext.getCurrentInstance().addMessage("messages", msg);
            init();
            e.printStackTrace();
        }
        return "";
    }

    /*metodos de ayuda*/
    public String convertirFecha(Date fecha) {
        if (fecha == null) {
            return "";
        } else {
            String patron = "dd/MM/yyyy";
            SimpleDateFormat formato = new SimpleDateFormat(patron);
            return (formato.format(fecha));
        }
    }

    public String convertirEstado(String estado) {
        if ("0".equals(estado)) {
            return "Inactivo";
        } else {
            return "Activo";
        }
    }

    public void verForm() {
        form = true;
    }

    public void verTabla() {
        form = false;
    }

    public void renderizarItem() {
        idCategoria=evento.getCategoriaID().getIdCategoria().intValue();
        edit = true;
    }

    public EventoService getEventoService() {
        return eventoService;
    }

    public void setEventoService(EventoService eventoService) {
        this.eventoService = eventoService;
    }

    public List<Evento> getEventoList() {
        return eventoList;
    }

    public void setEventoList(List<Evento> eventoList) {
        this.eventoList = eventoList;
    }

    public Evento getEvento() {
        try {
            if (evento == null) {
                evento = new Evento();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("Error inicializando objeto");
        }
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isForm() {
        return form;
    }

    public void setForm(boolean form) {
        this.form = form;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<SelectItem> getItemsCategoria() {
        return itemsCategoria;
    }

    public void setItemsCategoria(List<SelectItem> itemsCategoria) {
        this.itemsCategoria = itemsCategoria;
    }

    public CategoriaService getCategoriaService() {
        return categoriaService;
    }

    public void setCategoriaService(CategoriaService categoriaService) {
        this.categoriaService = categoriaService;
    }

}
