
package com.global.test.persistence.services;

import com.global.test.modelo.Categoria;
import java.util.List;

/**
 *
 * CategoriaService
 *
 *
 * @author ChrisV_G
 * @since 14/05/2017
 *
 *
 * Historia de Modificaciones
 * -------------------------------------------------- 
 * Autor             Fecha          Modificacion 
 * ----------------- -------------- ------------------
 */
public interface CategoriaService {


    public void persist(Categoria categoria) throws Exception;

    public void merge(Categoria categoria) throws Exception;

    public void delete(Categoria categoria) throws Exception;

    public List<Categoria> retrieveAll() throws Exception;

    public Categoria findByID(int idCategoria);

    

}