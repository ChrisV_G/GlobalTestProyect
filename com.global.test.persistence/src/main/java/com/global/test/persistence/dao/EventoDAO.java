package com.global.test.persistence.dao;

import com.global.test.modelo.Evento;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.global.test.modelo.Evento;
import java.util.HashMap;
import java.util.Map;

@Repository
public class EventoDAO {

    private final RowMapper<Evento> rowMapper;

    @Autowired
    private CategoriaDAO categoriaDAO;
    
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public EventoDAO(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

        this.rowMapper = new RowMapper<Evento>() {
            @Override
            public Evento mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

                Evento evento = new Evento();
                evento.setFecha(resultSet.getDate("FECHA"));
                evento.setIdEvento(resultSet.getLong("ID_EVENTO"));
                evento.setLugar(resultSet.getString("LUGAR"));
                evento.setNombre(resultSet.getString("NOMBRE"));
                evento.setCategoriaID(categoriaDAO.findByID(resultSet.getInt("CATEGORIA_ID")));
                return evento;
            }
        };
    }

    public List<Evento> getByCategoria(long eventoID) {
        try {
            String sql = "SELECT * FROM EVENTO WHERE CATEGORIA_ID=:CATEGORIA_ID";
            MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            namedParameters.addValue("CATEGORIA_ID", eventoID);
            List<Evento> list = namedParameterJdbcTemplate.query(sql, namedParameters, rowMapper);

            return list;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Evento> selectAll() throws RuntimeException {
        String sql = "SELECT * FROM evento";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        List<Evento> list = namedParameterJdbcTemplate.query(sql, namedParameters, rowMapper);
        return list;
    }
    
    
    public boolean persist(Evento evento) {
        try {
            String sql = "INSERT INTO EVENTO (ID_EVENTO,LUGAR,NOMBRE,FECHA,CATEGORIA_ID) "
                    + "VALUES (SQ_EVENTO.nextval,:LUGAR,:NOMBRE,:FECHA,:CATEGORIA_ID)";
            Map<String, Object> namedParameters = new HashMap<String, Object>();
            namedParameters.put("LUGAR", evento.getLugar());
            namedParameters.put("NOMBRE", evento.getNombre());
            namedParameters.put("FECHA", evento.getFecha());
            namedParameters.put("CATEGORIA_ID", evento.getCategoriaID().getIdCategoria());
            namedParameterJdbcTemplate.update(sql, namedParameters);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return true;

    }

    public boolean merge(Evento evento) {
        try {
            String sql = "UPDATE EVENTO SET "
                    + "LUGAR=:LUGAR, NOMBRE =:NOMBRE, "
                    + "FECHA=:FECHA, CATEGORIA_ID=:CATEGORIA_ID "
                    + "WHERE ID_EVENTO=:SQ_EVENTO";
            Map<String, Object> namedParameters = new HashMap<String, Object>();
            namedParameters.put("SQ_EVENTO", evento.getIdEvento());
            namedParameters.put("LUGAR", evento.getLugar());
            namedParameters.put("NOMBRE", evento.getNombre());
            namedParameters.put("FECHA", evento.getFecha());
            namedParameters.put("CATEGORIA_ID", evento.getCategoriaID().getIdCategoria());
            namedParameterJdbcTemplate.update(sql, namedParameters);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return true;
    }

    public boolean delete(Evento evento) {
        try {
            String sql = "DELETE EVENTO WHERE ID_EVENTO=:SQ_EVENTO";
            Map<String, Object> namedParameters = new HashMap<String, Object>();
            namedParameters.put("SQ_EVENTO", evento.getIdEvento());
            namedParameterJdbcTemplate.update(sql, namedParameters);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return true;
    }

    public List<Evento> retrieveByCategoria(String categoriaID) {
        String sql = "SELECT * FROM EVENTO WHERE CATEGORIA_ID=:CATEGORIA_ID";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("CATEGORIA_ID", categoriaID);
        List<Evento> list = namedParameterJdbcTemplate.query(sql, namedParameters, rowMapper);
        return list;
    }
    
    public Evento selectByname(String nombre) throws RuntimeException {
        String sql = "SELECT * FROM EVENTO WHERE NOMBRE=:NOMBRE";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("NOMBRE", nombre);
        List<Evento> list = namedParameterJdbcTemplate.query(sql, namedParameters, rowMapper);
        return list.get(0);
    }
    
}
