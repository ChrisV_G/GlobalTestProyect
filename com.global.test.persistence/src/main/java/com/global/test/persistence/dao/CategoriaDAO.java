package com.global.test.persistence.dao;

import com.global.test.modelo.Categoria;
import com.global.test.modelo.Evento;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CategoriaDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final RowMapper<Categoria> rowMapper;

    @Autowired
    private EventoDAO eventoDAO;

    @Autowired
    public CategoriaDAO(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

        this.rowMapper = new RowMapper<Categoria>() {
            @Override
            public Categoria mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

                Categoria categoria = new Categoria();
                categoria.setEstado(resultSet.getString("ESTADO"));
                categoria.setIdCategoria(resultSet.getLong("ID_CATEGORIA"));
                categoria.setNombre(resultSet.getString("NOMBRE"));
                return categoria;
            }
        };
    }

    public Categoria findByID(int id) {
        String sql = "SELECT * FROM CATEGORIA wherE ID_CATEGORIA=:ID_CATEGORIA";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("ID_CATEGORIA", id);
        List<Categoria> list = namedParameterJdbcTemplate.query(sql, namedParameters, rowMapper);
        if (list.size() > 0) {
            return list.get(0);
        } else {
            return new Categoria();
        }
    }

    public List<Categoria> selectAll() throws RuntimeException {
        String sql = "SELECT * FROM CATEGORIA";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        List<Categoria> list = namedParameterJdbcTemplate.query(sql, namedParameters, rowMapper);
        for (Categoria categoria : list) {
            categoria.setEventos(eventoDAO.getByCategoria(categoria.getIdCategoria()));
        }
        return list;
    }

    public boolean persist(Categoria categoria) {
        try {
            String sql = "INSERT INTO CATEGORIA (ID_CATEGORIA,ESTADO,NOMBRE) VALUES (SQ_CATEGORIA.nextval,:ESTADO,:NOMBRE)";
            Map<String, Object> namedParameters = new HashMap<String, Object>();
            namedParameters.put("ESTADO", categoria.getEstado());
            namedParameters.put("NOMBRE", categoria.getNombre());
            namedParameterJdbcTemplate.update(sql, namedParameters);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return true;

    }

    public boolean merge(Categoria categoria) {
        try {
            String sql = "UPDATE CATEGORIA SET ESTADO=:ESTADO,NOMBRE =:NOMBRE WHERE ID_CATEGORIA=:SQ_CATEGORIA";
            Map<String, Object> namedParameters = new HashMap<String, Object>();
            namedParameters.put("ESTADO", categoria.getEstado());
            namedParameters.put("NOMBRE", categoria.getNombre());
            namedParameters.put("SQ_CATEGORIA", categoria.getIdCategoria());
            namedParameterJdbcTemplate.update(sql, namedParameters);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return true;
    }

    public boolean delete(Categoria categoria) {
        try {
            String sql = "DELETE CATEGORIA WHERE ID_CATEGORIA=:SQ_CATEGORIA";
            Map<String, Object> namedParameters = new HashMap<String, Object>();
            namedParameters.put("SQ_CATEGORIA", categoria.getIdCategoria());
            namedParameterJdbcTemplate.update(sql, namedParameters);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return true;
    }

    public Categoria selectByname(String nombre) throws Exception {
        String sql = "SELECT * FROM CATEGORIA WHERE NOMBRE =:NOMBRE";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("NOMBRE", nombre);
        List<Categoria> list = namedParameterJdbcTemplate.query(sql, namedParameters, rowMapper);
        return list.get(0);
    }
}
