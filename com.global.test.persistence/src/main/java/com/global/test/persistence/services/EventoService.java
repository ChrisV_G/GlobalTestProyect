package com.global.test.persistence.services;

import com.global.test.modelo.Evento;
import java.util.List;

/**
 *
 * EventoService
 *
 *
 * @author ChrisV_G
 * @since 14/05/2017
 *
 *
 * Historia de Modificaciones --------------------------------------------------
 * Autor Fecha Modificacion ----------------- -------------- ------------------
 */
public interface EventoService {

    public void persist(Evento evento) throws Exception;

    public void merge(Evento evento) throws Exception;

    public void delete(Evento evento) throws Exception;

    public List<Evento> retrieveAll() throws Exception;

    public List<Evento> retrieveByCategoria(String categoriaID) throws Exception;
}
