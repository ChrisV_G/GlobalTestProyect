package com.global.test.services.impl;

import com.global.test.modelo.Evento;
import com.global.test.persistence.dao.EventoDAO;
import com.global.test.persistence.services.EventoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * EventoServiceImpl
 *
 *
 * @author ChrisV_G
 * @since 14/05/2017
 *
 *
 * Historia de Modificaciones --------------------------------------------------
 * Autor Fecha Modificacion ----------------- -------------- ------------------
 */
@Service("eventoService")
public class EventoServiceImpl implements EventoService {

    @Autowired
    private EventoDAO eventoDAO;

    @Override
    public void persist(Evento evento) throws Exception {
        try {
            eventoDAO.persist(evento);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void merge(Evento evento) throws Exception {
        try {
            eventoDAO.merge(evento);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void delete(Evento evento) throws Exception {
        try {
            eventoDAO.delete(evento);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Evento> retrieveAll() throws Exception {
        return eventoDAO.selectAll();
    }

    @Override
    public List<Evento> retrieveByCategoria(String categoriaID) throws Exception {
        try {
            return eventoDAO.retrieveByCategoria(categoriaID);
        } catch (Exception e) {
            throw e;
        }
    }

}
