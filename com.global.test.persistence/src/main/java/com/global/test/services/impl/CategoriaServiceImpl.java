package com.global.test.services.impl;

import com.global.test.modelo.Categoria;
import com.global.test.persistence.dao.CategoriaDAO;
import com.global.test.persistence.services.CategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * CategoriaServiceImpl
 *
 *
 * @author ChrisV_G
 * @since 14/05/2017
 *
 *
 * Historia de Modificaciones --------------------------------------------------
 * Autor Fecha Modificacion ----------------- -------------- ------------------
 */
@Service("categoriaService")
public class CategoriaServiceImpl implements CategoriaService {

    @Autowired
    private CategoriaDAO categoriaDAO;

    @Override
    public void persist(Categoria categoria) throws Exception {
        try {
            categoriaDAO.persist(categoria);
        } catch (Exception e) {
            throw e;
        }

    }

    @Override
    public void merge(Categoria categoria) throws Exception {
        try {
            categoriaDAO.merge(categoria);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void delete(Categoria categoria) throws Exception {
        try {
            categoriaDAO.delete(categoria);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Categoria> retrieveAll() throws Exception {
        return categoriaDAO.selectAll();
    }

    @Override
    public Categoria findByID(int idCategoria) {
        try {
            return categoriaDAO.findByID(idCategoria);
        } catch (Exception e) {
            throw e;
        }
    }

}
